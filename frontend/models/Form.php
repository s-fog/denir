<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 26.05.2015
 * Time: 11:12
 */

namespace frontend\models;

use common\models\FormRequest;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\UploadedFile;

class Form extends Model
{
    public $name;
    public $phone;
    public $message;
    public $email;
    public $file;

    public $nophone = 0;
    public $type;
    public $BC;

    public function rules()
    {
        return [
            ['phone', 'required'],
            ['email', 'email'],
            ['file', 'file'],
            ['BC', 'compare', 'compareValue' => ''],
            [$this->attributes(), 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Тип заявки',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            'email' => 'Email',
            'file' => 'Файл',
        ];
    }

    public function formName()
    {
        return '';
    }

    public function sendForm()
    {
        $info = [];
        foreach ($this->attributes() as $attribute) {
            if ($attribute != 'emails' && $attribute != 'nophone' && strlen($this->$attribute)) {
                $info[] = $this->getAttributeLabel($attribute) . ': ' . $this->$attribute;
            }
        }
        $info = implode("\r\n", $info);
        $info .= "\r\n" . 'Сообщение отправлено со страницы: ' . $_SERVER['HTTP_REFERER'];

        if (YII_ENV != 'dev') {
            $emails = false;

            if ($emails) {
                $mailer = Yii::$app->mailer->compose()
                    ->setTo(explode(',', $emails))
                    ->setSubject($this->type)
                    ->setHtmlBody($info);

                if ($file = UploadedFile::getInstanceByName('file')) {
                    $mailer->embed($file->tempName, ['fileName' => $file->name]);
                }

                $mailer->send();
            }

//            return $this->sendLead($info);
            return true;

            if (false) {
                if ($file = UploadedFile::getInstanceByName('file')) {
                    $filename = md5(uniqid()) . '.' . $file->extension;
                    $file->saveAs(Yii::getAlias('@frontend/web/uploads/' . $filename));
                    $info .= "\r\nСсылка на файл: " . 'http://карты-для-тахографа.рф/uploads/' . $filename;
                }
            }
        } else {
            if ($file = UploadedFile::getInstanceByName('file')) {
                $filename = md5(uniqid()) . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@frontend/web/uploads/' . $filename));
                $info .= "\r\nСсылка на файл: " . Html::a($filename, 'http://http://карты-для-тахографа.рф/uploads/' . $filename);
            }

            $mailer = Yii::$app->mailer->compose()
                ->setTo('test@sitecon.net')
                ->setFrom('noreply@' . Yii::$app->request->serverName)
                ->setSubject($this->type)
                ->setHtmlBody(nl2br($info));

            return $mailer->send();
        }
    }

    protected function sendLead($info)
    {
        $project = Yii::$app->context->lead_key;
        $address = Yii::$app->context->lead_server;

        if (!$project || !$address) {
            return false;
        }

        $send = [
            'key' => $project,
            'type' => $this->type,
            'source' => Yii::$app->session->get('traffic'),
            'term' => '',
            'phone' => $this->phone,
            'contact' => $this->name,
            'info' => $info,
            'options' => '',
            'device' => '',
        ];

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false, // Skip SSL cert verify - эту нужно будет убрать когда будет сертификат
            CURLOPT_SSL_VERIFYHOST => false, // Skip SSL cert verify - эту нужно будет убрать когда будет сертификат
            CURLOPT_URL => $address,
            CURLOPT_POST => true, // MIME: application/x-www-form-urlencoded
            CURLOPT_POSTFIELDS => $send,
        ]);
        $result = curl_exec($curl);
        curl_close($curl);

        return ['result' => $result, 'send' => $send];
    }
}