<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 22.03.2016
 * Time: 10:30
 */

namespace frontend\controllers;


use frontend\models\PaymentForm;
use Yii;
use yii\web\Controller;

class PageController extends Controller
{
    public function actionIndex()
    {
        return $this->renderPage();
    }

    public function actionView()
    {
        return $this->renderPage();
    }

    protected function renderPage()
    {
        $content = Yii::$app->content;
        $viewFile = 'view.php';
        $aliasViewFile = $content->alias . '.php';

        if (file_exists($this->viewPath . '/' . $aliasViewFile)) {
            $viewFile = $aliasViewFile;
        }

        return $this->render($viewFile, [
            'content' => $content,
        ]);
    }
}