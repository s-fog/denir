<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=1280, initial-scale=1">
    <?= Yii::$app->content->seoTags() ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->content->seo_title ?: $this->title) ?> - <?= Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="mainHeader">
    <div class="container">
        <div class="mainHeader__inner">
            <a href="#" class="mainHeader__logo"></a>
            <div class="mainHeader__left">
                <ul class="menu1">
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Мастерская</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Отзывы клиентов</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Оплата и доставка</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Подарочные сертификаты</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Видео обзоры</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Контакты</a>
                    </li>
                </ul>
                <ul class="menu2">
                    <li class="menu2__item"><a href="#" class="menu2__link">Портрет по фотографии</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Картина на заказ</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Модульные картины</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Картина в подарок</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Шаржи</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Роспись стен</a></li>
                </ul>
            </div>
            <div class="mainHeader__right">
                <a href="#" class="mainHeader__rightItem phone">
                    <span>8 800 505 2511</span>
                    Звонок бесплатный
                </a>
                <a href="#" class="mainHeader__rightItem address" style="flex: 0 0 34%; width: 34%;">
                    <span>г. Киев, переулок<br>Куреневский 15</span>
                </a>
                <div class="mainHeader__rightItem time">
                    пн-сб 09:00–21:00
                    <span>вс - выходной</span>
                </div>
                <div class="mainHeader__rightItem">
                    <a href="#callback" data-fancybox class="button1" style="width: 100%;">Заказать звонок</a>
                </div>
            </div>
        </div>
        <ul class="menu3">
            <li class="menu3__item"><a href="#" class="menu3__link">Каталог картин</a></li>
            <li class="menu3__item"><a href="#" class="menu3__link">Каталог репродукций</a></li>
            <li class="menu3__item"><a href="#" class="menu3__link">Каталог модульных картин</a></li>
            <li class="menu3__item"><a href="#" class="menu3__link">Каталог образов</a></li>
            <li class="menu3__item"><a href="#" class="menu3__link">Каталог багетов</a></li>
        </ul>
    </div>
</div>

<?= $content ?>


<div class="footer">
    <div class="container">
        <div class="footer__inner">
            <div class="footer__item">
                <ul class="menu3 menu3_footer">
                    <li class="menu3__item"><a href="#" class="menu3__link">Каталог картин</a></li>
                    <li class="menu3__item"><a href="#" class="menu3__link">Каталог репродукций</a></li>
                    <li class="menu3__item"><a href="#" class="menu3__link">Каталог модульных картин</a></li>
                    <li class="menu3__item"><a href="#" class="menu3__link">Каталог образов</a></li>
                    <li class="menu3__item"><a href="#" class="menu3__link">Каталог багетов</a></li>
                </ul>
            </div>
            <div class="footer__item">
                <ul class="menu2 menu2_footer">
                    <li class="menu2__item"><a href="#" class="menu2__link">Портрет по фотографии</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Картина на заказ</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Модульные картины</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Картина в подарок</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Шаржи</a></li>
                    <li class="menu2__item"><a href="#" class="menu2__link">Роспись стен</a></li>
                </ul>
            </div>
            <div class="footer__item">
                <ul class="menu1 menu1_footer">
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Мастерская</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Отзывы клиентов</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Оплата и доставка</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Подарочные сертификаты</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Видео обзоры</a>
                    </li>
                    <li class="menu1__item">
                        <a href="#" class="menu1__link">Контакты</a>
                    </li>
                </ul>
            </div>
            <div class="footer__item">
                <div class="quickOrder">
                    <div class="quickOrder__header">Быстрый заказ:</div>
                    <div class="quickOrder__text">Отправляйте фото и уже через
                        4 часа вы получите возможные варианты будущей работы</div>
                    <a href="#" class="button2 button2_footer" style="margin-bottom: 5px;">Загрузить фото</a>
                    <a href="#raschet" data-fancybox class="button1 button1_padding button1_footer">Сделать просчет</a>
                </div>
            </div>
            <div class="footer__item">
                <a href="#" class="phone phone_footer">
                    <span>8 800 505 2511</span>
                    Звонок бесплатный
                </a>
                <br>
                <div class="time time_footer">
                    пн-сб 09:00–21:00
                    <span>вс - выходной</span>
                </div>
                <br>
                <a href="#" class="address address_footer">
                    <span>г. Киев, переулок<br>Куреневский 15</span>
                </a>
            </div>
        </div>
    </div>
    <div class="footer__line"></div>
    <div class="footer__bottom">© 2012 -2017 «Denir». Все права на любые материалы, опубликованные на сайте, защищены в соответствии с российским<br>
        и международным законодательством об авторском праве и смежных правах.</div>
</div>

<div id="work" class="work">

</div>
<form id="callback" class="popup">
    <div class="popup__header">Заказать звонок</div>
    <div class="form-group">
        <input type="text" name="name" placeholder="Имя" class="input">
    </div>
    <div class="form-group">
        <input type="text" name="phone" placeholder="Телефон" class="input">
    </div>
    <div class="form-group">
        <input type="text" name="email" placeholder="Почтовый ящик" class="input">
    </div>
    <button type="submit" class="button3">Заказать</button>
</form>
<form id="raschet" class="popup">
    <div class="popup__header">Сделать просчет</div>
    <div class="form-group">
        <input type="text" name="name" placeholder="Имя" class="input">
    </div>
    <div class="form-group">
        <input type="text" name="phone" placeholder="Телефон" class="input">
    </div>
    <div class="form-group">
        <input type="text" name="email" placeholder="Почтовый ящик" class="input">
    </div>
    <div class="form-group">
        <label class="file">
            <div class="button4">Загрузить фото</div>
            <input type="file" name="file">
        </label>
    </div>
    <button type="submit" class="button3">Заказать</button>
</form>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
