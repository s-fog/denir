<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/resources/';
    public $baseUrl = '@web/resources/';
    public $css = [
        'js/vendor/owl-carousel/owl-carousel/owl.carousel.css',
        'js/vendor/fancybox/dist/jquery.fancybox.min.css',
        'css/styles.css'
    ];
    public $js = [
        'js/vendor.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
