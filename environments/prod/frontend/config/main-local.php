<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '6g2WULlIq5x3gg55WN9IteQ86J8vxpHA',
            'csrfParam' => '_frontendCsrf',
        ],
        /*'user' => [
            'identityCookie' => [
                'name' => '_frontendUser',
                'path' => '/',
            ]
        ],*/
        'session' => [
            'name' => '_frontendSessionId',
            'savePath' => __DIR__ . '/../runtime/sessions'
        ]
    ],
];

return $config;
