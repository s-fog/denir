<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '6g2WULlIq5x3gg55WN9IteQ86J8vxpHA',
            'csrfParam' => '_frontendCsrf',
        ],
        /*'user' => [
            'identityCookie' => [
                'name' => '_frontendUser',
                'path' => '/',
            ]
        ],*/
        'session' => [
            'name' => '_frontendSessionId',
            'savePath' => __DIR__ . '/../runtime/sessions'
        ]
    ],
];

if (!YII_ENV_TEST) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
