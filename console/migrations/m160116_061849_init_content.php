<?php

use yii\db\Migration;

class m160116_061849_init_content extends Migration
{
    public function up()
    {
        $this->createTable('{{%content}}', [
            'id' => 'pk',
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'parent_id' => $this->integer(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
            'type' => $this->string()->notNull()->defaultValue('page'),
            'alias' => $this->string()->notNull(),
            'pagetitle' => $this->string()->notNull(),
            'menutitle' => $this->string(),
            'icon' => $this->string(),
            'image' => $this->string(),
            'introtext' => $this->text(),
            'text' => $this->text(),
            'seo_title' => $this->string(),
            'seo_description' => $this->text(),
            'seo_keywords' => $this->text(),
            'seo_text' => $this->text(),
            'published' => $this->boolean()->notNull()->defaultValue(1),
            'hidemenu' => $this->boolean()->notNull()->defaultValue(1),
            'show_in_tree' => $this->boolean()->notNull()->defaultValue(1),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
            'settings' => $this->binary(),
        ]);

        $this->createIndex('lft', '{{%content}}', 'lft');
        $this->createIndex('rgt', '{{%content}}', 'rgt');
        $this->createIndex('depth', '{{%content}}', 'depth');
        $this->createIndex('created_at', '{{%content}}', 'created_at');
        $this->createIndex('updated_at', '{{%content}}', 'updated_at');
        $this->createIndex('type', '{{%content}}', 'type');
        $this->createIndex('alias', '{{%content}}', 'alias', true);
        $this->createIndex('pagetitle', '{{%content}}', 'pagetitle');
        $this->createIndex('published', '{{%content}}', 'published');
        $this->createIndex('hidemenu', '{{%content}}', 'hidemenu');
        $this->createIndex('show_in_tree', '{{%content}}', 'show_in_tree');

        $this->insert('{{%content}}', [
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
            'parent_id' => 0,
            'created_at' => time(),
            'updated_at' => time(),
            'type' => 'page',
            'alias' => 'index',
            'pagetitle' => 'Главная',
        ]);


        $this->createTable('{{%tag}}', [
            'id' => 'pk',
            'name' => $this->string()->notNull(),
            'frequency' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ]);

        $this->createIndex('frequency', '{{%tag}}', 'frequency');


        $this->createTable('{{%content_has_tag}}', [
            'content_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('', '{{%content_has_tag}}', ['content_id', 'tag_id']);
        $this->addForeignKey('fk_content_has_tag_content', '{{%content_has_tag}}', 'content_id', 'content', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_content_has_tag_tag', '{{%content_has_tag}}', 'tag_id', 'tag', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%content_has_tag}}');
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%content}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
