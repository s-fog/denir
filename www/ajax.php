<div class="work__inner">
    <div class="work__left work__left_main" style="background-image: url(resources/img/bg1.png);">
        <div class="work__toInterier"><span>Картина в интерьере</span></div>
        <a href="resources/img/kaz.png" class="work__image" data-fancybox="workImage">
            <div class="work__imageInner">
                <img src="resources/img/kaz.png" alt="">
                <div class="work__imageLoop"></div>
            </div>
        </a>
    </div>
    <div class="work__left work__left_interier" style="background-image: url(resources/img/sl1.png);"></div>
    <div class="work__left work__left_review">
        <div class="work__reviewSlider">
            <a href="resources/img/rsli.png" class="work__sliderItem" data-fancybox="workSliderItem"><img src="resources/img/rsli.png" alt=""></a>
            <a href="resources/img/rsli.png" class="work__sliderItem" data-fancybox="workSliderItem"><img src="resources/img/rsli.png" alt=""></a>
            <a href="resources/img/rsli.png" class="work__sliderItem" data-fancybox="workSliderItem"><img src="resources/img/rsli.png" alt=""></a>
        </div>
    </div>
    <div class="work__right">
        <div class="work__faces">
            <a href="resources/img/face.png" data-fancybox="faces" class="work__face"><img src="resources/img/face.png" alt=""></a>
            <a href="resources/img/face.png" data-fancybox="faces" class="work__face"><img src="resources/img/face.png" alt=""></a>
            <a href="resources/img/face.png" data-fancybox="faces" class="work__face"><img src="resources/img/face.png" alt=""></a>
            <a href="resources/img/face.png" data-fancybox="faces" class="work__face"><img src="resources/img/face.png" alt=""></a>
        </div>
        <p><?php echo $_POST['workId']; ?> Заказ №43801<br>Дата: 15.02.2017</p>
        <p>Художник-дизайнер:<br>Валерия Ивахненко</p>
        <p class="work__rightBigText">Материал: Холст<br>Размер: 100 x 150 см.</p>
        <ul>
            <li>фотомонтаж</li>
            <li>замена фона</li>
            <li>прорисовка акрилом</li>
        </ul>
        <p class="work__rightBigText">Срок создания: 3 дня</p>
    </div>
</div>
<div class="work__showReview">отзыв клиента</div>
<div class="work__bottom">
    <div class="work__bottomLeft">
        <div class="work__bottomLeftInner">
            <div class="work__bottomRightText">Мы, студия Денир, презентовали картину сотрудникам службы безопасности офисного здания. Эмоции которые мы получили, превзошли наши ожидания,
                море позитивных впечатлений от людей которые были получателями, они благодарили и с их лиц не сходили улыбки. Приятно удивлять людей которые даже не ждут подарка. Всем счастья! </div>
            <hr>
            <div class="work__reviewWho">
                <div class="work__reviewWhoName">Сакирян Владимир</div>
                <div class="work__reviewWhoText">Киев&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Подарок сотрудникам службы безопасности</div>
            </div>
        </div>
    </div>
    <div class="work__bottomRight">
        <div class="work__bottomRightHeader">Хотите так же?</div>
        <div class="work__bottomRightText">Отправляйте фото и уже через
            4 часа вы получите возможные варианты будущей работы</div>
        <br>
        <br>
        <br>
        <img src="resources/img/arr.png" alt="">
        <a href="#" class="button2" style="margin-bottom: 10px;">Загрузить фото</a>
        <a href="#" class="button1 button1_padding">Сделать просчет</a>
        <br>
        <br>
        <a href="#" class="work__bottomRightLink">Доставка и оплата</a>
    </div>
</div>
<button data-fancybox-close="" class="fancybox-close-small"></button>