class Works {
    constructor(root) {
        this.root = root;

        this.workContainerWidth = 0;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            workContainer: $('#work'),
            fancyboxButtonLeft: $(),
            fancyboxButtonRight: $(),
            workLeft: $('.work__left'),
            workLeftInterier: $('.work__left_interier'),
            workLeftMain: $('.work__left_main'),
            workLeftReview: $('.work__left_review'),
            button: $('.work__showReview'),
            toInterier: $('.work__toInterier'),
            reviewContainer: $('.work__bottomLeftInner')
        }
    }

    _bindEvents() {
        $(window).resize(() => {
            this.controlButtonsToMiddle()
        });

        this.nodes.workContainer.on('click', '.'+this.nodes.toInterier.attr('class')+'', () => {
            this.nodes.workLeft.hide();
            this.nodes.workLeftInterier.show();
            this.nodes.button.text('Вернуться к работе');
            this.nodes.button.addClass('open');
        });

        this.nodes.workContainer.on('click', '.'+this.nodes.button.attr('class')+'', (event) => {
            if($(event.currentTarget).hasClass('open')) {
                this.nodes.workLeft.hide();
                this.nodes.workLeftMain.show();
                this.reviewShowHide('hide');
                this.changeButton(false);
            } else {
                this.reviewShowHide('show');
                this.changeButton(true);
            }
        });
    }

    _ready() {
        this.workContainerWidth = this.nodes.workContainer.width();

        $('[data-fancybox="works"]').fancybox({
            scrolling: false,
            baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
            '<div class="fancybox-bg"></div>' +
            '<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
            '<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
            '<div class="fancybox-controls">' +
            '<div class="fancybox-buttons">' +
            '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
            '</div>' +
            '</div>' +
            '<div class="fancybox-slider-wrap">' +
            '<div class="fancybox-slider"></div>' +
            '</div>' +
            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
            '</div>',
            onInit: () => {
                this.nodes.fancyboxButtonLeft = $('.fancybox-button--left');
                this.nodes.fancyboxButtonRight = $('.fancybox-button--right');
            },
            beforeLoad: (instance, slide) => {
                let workId = $(slide.opts.$orig).data('id');
                this.loadAjaxContent(workId);
                this.controlButtonsToMiddle();
            },
            beforeMove: () => {
                this.loading('show');
            }
        });

        $('[data-fancybox="faces"], [data-fancybox="workImage"], [data-fancybox="workSliderItem"]').fancybox({
            baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
            '<div class="fancybox-bg"></div>' +
            '<div class="fancybox-slider-wrap">' +
            '<div class="fancybox-slider"></div>' +
            '</div>' +
            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
            '</div>'
        });

        this.initSlider();
    }

    initSlider() {
        $('.work__reviewSlider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false
        });
    };

    controlButtonsToMiddle() {
        let height = this.nodes.fancyboxButtonLeft.height();
        let windowHeight = $(window).height();
        let middleOffset = (windowHeight - height) / 2;
        let windowWidth = $(window).width();
        let sideOffset = (windowWidth - this.workContainerWidth) / 4;

        this.nodes.fancyboxButtonLeft.css({"top": middleOffset + "px", "left": sideOffset + "px"});
        this.nodes.fancyboxButtonRight.css({"top": middleOffset + "px", "right": sideOffset + "px"});
    };

    changeButton(back) {
        if(back) {
            this.nodes.button.text('Вернуться к работе');
            this.nodes.button.addClass('open');
            this.nodes.workLeft.hide();
            this.nodes.workLeftReview.show();
        } else {
            this.nodes.button.text('Отзыв клиента');
            this.nodes.button.removeClass('open');
            this.nodes.workLeft.hide();
            this.nodes.workLeftMain.show();
        }
    };

    reviewShowHide(mode) {
        if(mode == 'show') {
            this.nodes.reviewContainer.show();
        } else {
            this.nodes.reviewContainer.hide();
        }
    };

    loadAjaxContent(workId) {
        let data = 'workId=' + workId;

        $.post('ajax.php', data, (response) => {
            this.nodes.workContainer.html(response);
            this.loading('hide');

            setTimeout(() => {
                this._cacheNodes();
                this.initSlider();
            }, 100);
        });
    };

    loading(mode) {
        if (mode == 'show') {
            this.nodes.workContainer.addClass('work_loading');
        } else {
            this.nodes.workContainer.removeClass('work_loading');
        }
    };
}

class Sliders {
    constructor(root) {
        this.root = root;

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            handle: $(),
            widthFrom: $('[name="widthFrom"]'),
            widthTo: $('[name="widthTo"]'),
            priceFrom: $('[name="priceFrom"]'),
            priceTo: $('[name="priceTo"]'),
        }
    }

    _bindEvents() {

    }

    _ready() {
        $('.filterWidth').slider({
            range: true,
            min: 1,
            max: 12,
            values: [ 1, 12 ],
            create: (event, ui) => {
                this.nodes.handle = $('.filterWidth .ui-slider-handle');
                this.nodes.handle.append('<span class="ui-slider-handle-text">');
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(this.nodes.widthFrom.val());
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(this.nodes.widthTo.val());
            },
            slide: ( event, ui ) => {
                this.nodes.handle = $('.filterWidth .ui-slider-handle');
                this.nodes.widthFrom.val(ui.values[0]);
                this.nodes.widthTo.val(ui.values[1]);
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(ui.values[0]);
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(ui.values[1]);
            }
        });

        $('.filterPrice').slider({
            range: true,
            min: 500,
            max: 4350,
            values: [ 500, 4350 ],
            create: (event, ui) => {
                this.nodes.handle = $('.filterPrice .ui-slider-handle');
                this.nodes.handle.append('<span class="ui-slider-handle-text">');
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(this.nodes.priceFrom.val());
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(this.nodes.priceTo.val());
            },
            slide: ( event, ui ) => {
                this.nodes.handle = $('.filterPrice .ui-slider-handle');
                this.nodes.priceFrom.val(ui.values[0]);
                this.nodes.priceTo.val(ui.values[1]);
                this.nodes.handle.eq(0).find('.ui-slider-handle-text').text(ui.values[0]);
                this.nodes.handle.eq(1).find('.ui-slider-handle-text').text(ui.values[1]);
            }
        });
    }
}

class Application {
    constructor() {
        this._mainScripts();
        this._initClasses();
    }

    _mainScripts() {
        /*$('input[name="phone"], input[type="tel"]').inputmask('+7 (999) 999-99-99');*/

        $('.slider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false
        });

        $('[data-fancybox*="review"]').fancybox();

        $('.present__slider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false,
            pagination: true
        });

        $('.contactPage__slider, .contactPage__personalSlider, .reviews__itemSlider').owlCarousel({
            items: 1,
            navigation: true,
            navigationText: false,
            pagination: false
        });

        $('.file [type="file"]').change(function(event) {
            var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '')
            $('.file .button4').text(file_name);
        });

        $( ".sort__select" ).selectmenu().addClass();

        $('[data-fancybox="bagetPopup"]').fancybox({
            beforeLoad: (instance, slide) => {
                let bagetPopup = $('#bagetPopup');
                let parent = $(slide.opts.$orig).parents('.catalogBagets__item');

                let img = parent.find('.catalogBagets__itemImage img').attr('src');
                let html = parent.find('.catalogBagets__itemInfoInner').html();

                bagetPopup.find('.bagetPopup__rightInner').html(html);
                bagetPopup.find('.bagetPopup__left img').attr('src', img);
            }
        });

        ymaps.ready(function () {
            var myMap = new ymaps.Map('map', {
                    center: [55.751574, 37.573856],
                    zoom: 9
                }, {
                    searchControlProvider: 'yandex#search'
                }),

            // Создаём макет содержимого.
                MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                    '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                ),

                myPlacemarkWithContent = new ymaps.Placemark([55.751574, 37.573856], {
                    hintContent: 'Собственный значок метки с контентом',
                    balloonContent: 'А эта — новогодняя',
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#imageWithContent',
                    // Своё изображение иконки метки.
                    iconImageHref: '/resources/img/baloon.png',
                    // Размеры метки.
                    iconImageSize: [53, 74],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-22, -74],
                    // Макет содержимого.
                    iconContentLayout: MyIconContentLayout
                });
            myMap.behaviors.disable('scrollZoom');

            myMap.geoObjects
                .add(myPlacemarkWithContent);
        });
    }

    _initClasses() {
        new Works();
        new Sliders();
    }
}

(function () {
    new Application();
})();