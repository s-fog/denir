<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 10.02.2016
 * Time: 21:12
 */

namespace common\components\content\controllers;


use common\models\Content;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ContentController extends Controller
{
//    public $layout = '@backend/views/content/layout';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['update', 'id' => Content::find()->one()->id]);
    }

    public function actionCreate($parent)
    {
        $root = $this->findModel($parent);

        $type = 'common\models\\' . Inflector::id2camel($root->type);

        $className = Content::className();

        if (class_exists($type)) {
            $className = $type;
        }

        /** @var Content|ActiveRecord $content */
        $content = new $className([
            'pagetitle' => Yii::$app->request->post('pagetitle'),
            'type' => $root->type,
        ]);


        if ($content->validate()) {
            $content->appendTo($root);
            $content->save();

            return $this->redirect(['update', 'id' => $content->id]);
        }

        return $this->render('update', ['model' => $content]);
    }

    public function actionUpdate($id)
    {
        $this->layout = '@backend/views/content/layout';

        $content = $this->findClass($id);
//        $content->scenario = 'update';

        $post = Yii::$app->request->post();

        if ($content->load($post) && $content->save()) {
            if (Yii::$app->request->isPjax) {
                return $this->renderAjax('update', ['model' => $content]);
            }

            Yii::$app->session->setFlash('success', 'Успешно сохранено');

            return $this->redirect(['update', 'id' => $content->id]);
        }

        if (Yii::$app->request->isPjax) {
            return $this->renderAjax('update', ['model' => $content]);
        }

        return $this->render('update', ['model' => $content]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithChildren();

        if (!Yii::$app->request->isAjax) {
            Yii::$app->session->setFlash('success', 'Страница удалена!');

            return $this->redirect(['index']);
        }
    }

    public function actionMove()
    {
        $post = Yii::$app->request->post();
        $node = $this->findModel($post['nodeKey']);
        $otherNode = $this->findModel($post['otherNodeKey']);

        if ($post['mode'] == 'over') {
            $action = 'appendTo';
        } else {
            $action = 'insert' . ucfirst($post['mode']);
        }

        $otherNode->$action($node);

        $parentNode = $otherNode->parents(1)->one();
        $otherNode->parent_id = $parentNode->id;

        if ($parentNode->id != 1 && $node != $parentNode->type) {
            $otherNode->type = $parentNode->type;

            $className = 'common\models\\' . Inflector::id2camel($parentNode->type);

            if (class_exists($className) && !$className::find(['id' => $otherNode->id])->exists()) {
                /** @var ActiveRecord $classModel */
                $classModel = new $className([
                    'id' => $otherNode->id,
                ]);

                $classModel->save();
            }
        }

        $otherNode->save();
    }

    public function actionTree($id, $content_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $data = [];

        $root = $this->findModel(1);
        $content = Content::findOne($content_id);

        $data[] = [
            'id' => 1, 'text' => $root->menutitle, 'parent' => '#', 'state' => [
                'opened' => true,
                'selected' => $content_id == 1,
            ],
        ];

        /** @var Content $child */
        foreach ($root->children()->inTree()->each() as $child) {
            $data[] = [
                'id' => $child->id,
                'text' => $child->menutitle . " <sup>($child->id)</sup>",
                'parent' => $child->parent_id,
                'state' => [
                    'opened' => $content->isChildOf($child) || $content->id == $child->id,
                    'selected' => $content->id == $child->id,
                ],
            ];
        }

        return $data;

    }

    /**
     * @param Content $root
     * @param Content $content
     * @return array
     */
    private function getTreeChildren($root, $content)
    {
        $data = [];

        /** @var Content $child */
        foreach ($root->children(1)->inTree()->each() as $child) {
            $data[] = [
                'id' => $child->id,
                'text' => $child->menutitle . " <sup>($child->id)</sup>",
                'children' => $child->isLeaf() ? [] : $this->getTreeChildren($child, $content),
                'state' => [
                    'opened' => $content->isChildOf($child) || $content->id == $child->id,
                    'selected' => $content->id == $child->id,
                ],
            ];
        }

        return $data;
    }


    /**
     * @param $id
     * @return null|Content
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if ($model = Content::findOne($id)) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findClass($id)
    {
        if ($model = Content::findOne($id)) {
            if (strlen($model->type)) {
                $type = 'common\models\\' . Inflector::id2camel($model->type);

                if (!class_exists($type)) {
                    return $model;
                }

                $class = $type::findOne($id);

                if ($class === null) {
                    return $model;
                } else {
                    return $class;
                }
            }
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}