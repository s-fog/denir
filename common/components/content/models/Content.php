<?php

namespace common\components\content\models;

use backend\components\EncodingFixBehavior;
use baibaratsky\yii\behaviors\model\SerializedAttributes;
use creocoder\nestedsets\NestedSetsBehavior;
use creocoder\taggable\TaggableBehavior;
use himiklab\sitemap\behaviors\SitemapBehavior;
use himiklab\thumbnail\EasyThumbnailImage;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $parent_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $type
 * @property string $alias
 * @property string $pagetitle
 * @property string $menutitle
 * @property string $icon
 * @property string $image
 * @property string $introtext
 * @property string $text
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_text
 * @property integer $published
 * @property integer $hidemenu
 * @property integer $show_in_tree
 * @property integer $deleted
 * @property array $settings
 *
 * @property string $tagValues
 *
 * @property ContentHasTag[] $contentHasTags
 * @property Tag[] $tags
 * @property Content $parent
 *
 * @property string $uri
 * @property ActiveRecord $typeModel
 *
 * @method makeRoot()
 * @method prependTo($node)
 * @method appendTo($node)
 * @method insertBefore($node)
 * @method insertAfter($node)
 * @method ContentQuery parents($level = null)
 * @method ContentQuery leaves($level = null)
 * @method ContentQuery children($level = null)
 * @method boolean isChildOf($node)
 * @method boolean isRoot()
 * @method boolean isLeaf()
 * @method integer|boolean deleteWithChildren()
 *
 * @method getTagValues($asArray)
 */
class Content extends \yii\db\ActiveRecord
{
    private $_typeModel;

    public function __get($name)
    {
        switch ($name) {
            case 'menutitle':
            case 'seo_title':
                return $this->getAttribute($name) ?: $this->pagetitle;
            default:
                return parent::__get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'pagetitle',
                'slugAttribute' => 'alias',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            'timestamp' => TimestampBehavior::className(),
            NestedSetsBehavior::className(),
            'serializedAttributes' => [
                'class' => SerializedAttributes::className(),
                'attributes' => ['settings'],
            ],
            [
                'class' => EncodingFixBehavior::className(),
                'attributes' => ['settings', 'image'],
            ],
            TaggableBehavior::className(),
            'sitemap' => [
                'class' => SitemapBehavior::className(),
                'scope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select(['id', 'alias', 'updated_at']);
                    $model->andWhere(['published' => 1, 'deleted' => 0])->andWhere(['!=', 'id', 1]);
                },
                'dataClosure' => function ($model) {
                    /** @var self $model */
                    return [
                        'loc' => Url::to($model->alias, true),
                        'lastmod' => date('Y-m-d', $model->updated_at),
                        'changefreq' => SitemapBehavior::CHANGEFREQ_WEEKLY,
                        'priority' => 0.8,
                    ];
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pagetitle'], 'required'],
            [['lft', 'rgt', 'depth', 'parent_id', 'created_at', 'updated_at', 'published', 'hidemenu', 'show_in_tree'], 'integer'],
            [['introtext', 'text', 'seo_description', 'seo_keywords', 'seo_text'], 'string'],
            [['type', 'alias', 'pagetitle', 'menutitle', 'icon', 'image', 'seo_title'], 'string', 'max' => 255],
            [['alias'], 'unique', 'on' => 'update'],
            [['settings', 'tagValues'], 'safe'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'created_at' => 'Дата создания',
            'updated_at' => 'Последнее обновление',
            'type' => 'Тип страницы',
            'alias' => 'Адрес',
            'pagetitle' => 'Заголовок',
            'menutitle' => 'Пункт меню',
            'icon' => 'Иконка',
            'image' => 'Изображение',
            'introtext' => 'Вступительный текст',
            'text' => 'Текст',
            'published' => 'Опубликован',
            'hidemenu' => 'Не показывать в меню',
            'show_in_tree' => 'Показывать в структуре',
            'seo_title' => 'Title',
            'seo_description' => 'Description',
            'seo_keywords' => 'Keywords',
            'seo_text' => 'SEO текст',
            'tagValues' => 'Теги',
        ];
    }

    public function getUri()
    {
        return $this->id == 1 ? '/' : Url::to(['content/view', 'alias' => $this->alias]);
    }

    public function getDatetime()
    {
        return date('d.m.Y', $this->created_at);
    }

    public function setDatetime($value)
    {
        $this->created_at = strtotime($value);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentHasTags()
    {
        return $this->hasMany(ContentHasTag::className(), ['content_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('content_has_tag', ['content_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(Content::className(), ['id' => 'parent_id']);
    }

    /**
     * @inheritdoc
     * @return ContentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContentQuery(get_called_class());
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $parent = $this->parents(1)->one();
            $this->parent_id = $parent->id;
        }
    }

    public function getTypeModel()
    {
        if ($this->_typeModel === null) {
            /** @var ActiveRecord $class */
            $class = 'common\models\\' . Inflector::id2camel($this->type);
            $this->_typeModel = $class::findOne($this->id);
        }

        return $this->_typeModel;
    }

    /**
     * @param int $width
     * @param int $height
     * @param array $options
     * @return string
     */
    public function getThumb($width, $height, $options = [])
    {
        return $this->image ? EasyThumbnailImage::thumbnailImg(Yii::getAlias('@www' . $this->image), $width, $height, EasyThumbnailImage::THUMBNAIL_OUTBOUND, $options) : null;
    }

    public function getThumbUrl($width, $height)
    {
        return $this->image ? EasyThumbnailImage::thumbnailFileUrl(Yii::getAlias('@www' . $this->image), $width, $height) : null;
    }

    public function getSetting($key)
    {
        return ArrayHelper::getValue($this->settings, $key);
    }
}
