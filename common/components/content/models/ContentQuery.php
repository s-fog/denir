<?php

namespace common\components\content\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use creocoder\taggable\TaggableQueryBehavior;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Content]].
 *
 * @see Content
 *
 * @method $this leaves()
 * @method $this children()
 * @method $this roots()
 *
 * @method ActiveQuery anyTagValues($tags)
 * @method ActiveQuery allTagValues($tags)
 * @method ActiveQuery relatedByTagValues($tags)
 */
class ContentQuery extends \yii\db\ActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
            TaggableQueryBehavior::className(),
        ];
    }

    public function published()
    {
        return $this->andWhere(['published' => 1]);
    }

    public function inMenu()
    {
        return $this->andWhere(['hidemenu' => 0]);
    }

    public function inTree()
    {
        return $this->andWhere(['show_in_tree' => 1]);
    }

    /**
     * @inheritdoc
     * @return Content[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Content|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
