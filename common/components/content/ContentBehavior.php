<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 11.02.2016
 * Time: 0:33
 */

namespace common\components\content;


use common\components\content\models\Content;
use yii\base\Behavior;
use yii\base\Model;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * Class ContentBehavior
 *
 * @package backend\components
 *
 * @property ActiveRecord $owner
 */
class ContentBehavior extends Behavior
{
    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (UnknownPropertyException $exception) {
            $model = $this->getContentModel();
            if ($model->hasAttribute($name) || $model->canGetProperty($name)) {
                return $model->$name;
            }

            throw $exception;
        }
    }

    public function __set($name, $value)
    {
        try {
            parent::__set($name, $value);
        } catch (UnknownPropertyException $exception) {
            $model = $this->getContentModel();
            if ($model->hasAttribute($name) || $model->canSetProperty($name)) {
                $model->$name = $value;
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function canGetProperty($name, $checkVars = true)
    {
        if (parent::canGetProperty($name, $checkVars)) {
            return true;
        }

        $model = $this->getContentModel();

        return $model->hasAttribute($name) || $model->canGetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function canSetProperty($name, $checkVars = true)
    {
        if (parent::canSetProperty($name, $checkVars)) {
            return true;
        }

        $model = $this->getContentModel();

        return $model->hasAttribute($name) || $model->canSetProperty($name, $checkVars);
    }

    /**
     * @inheritdoc
     */
    public function __call($name, $params)
    {
        $model = $this->getContentModel();
        if ($model->hasMethod($name)) {
            return call_user_func_array([$model, $name], $params);
        }

        return parent::__call($name, $params);
    }

    /**
     * @inheritdoc
     */
    public function hasMethod($name)
    {
        if (parent::hasMethod($name)) {
            return true;
        }
        $model = $this->getContentModel();

        return $model->hasMethod($name);
    }


    public function getContentModel()
    {
        $model = $this->owner->content;

        if ($model !== null) {
            return $model;
        }

        $model = new Content();
        $this->owner->populateRelation('content', $model);

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Model::EVENT_AFTER_VALIDATE => 'afterValidate',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterValidate($event)
    {
        if (!$this->owner->isRelationPopulated('content')) {
            return;
        }

        $model = $this->getContentModel();

        if (!$model->validate()) {
            $this->owner->addErrors($model->getErrors());
        }
    }

    public function beforeSave($event)
    {
        $model = $this->getContentModel();

        if ($this->owner->isNewRecord && !empty($this->owner->id)) {
            $model->id = $this->owner->id;
        }

        $model->type = strtolower((new \ReflectionClass($this->owner))->getShortName());
        $model->save(false);

        $this->owner->id = $model->getPrimaryKey();
    }

    public function afterDelete($event)
    {
        $model = $this->owner->content;
        if ($model !== null) {
            $model->delete();
        }
    }
}