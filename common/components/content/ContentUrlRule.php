<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 14.02.2016
 * Time: 2:26
 */

namespace common\components\content;


use common\models\Content;
use Yii;
use yii\base\Object;
use yii\web\Request;
use yii\web\UrlManager;
use yii\web\UrlRuleInterface;

class ContentUrlRule extends Object implements UrlRuleInterface
{
    public function createUrl($manager, $route, $params)
    {
        if ($route == 'content/view') {
            $alias = '';

            if (isset($params['alias'])) {
                $alias = $params['alias'];
                unset($params['alias']);
            } else if (isset($params['id'])) {
                if ($model = Content::findOne($params['id'])) {
                    $alias = trim($model->uri, '/');
                    unset($params['id']);
                }
            } else {
                return false;
            }

            return empty($params) ? $alias : $alias . '?' . http_build_query($params);
        }

        return false;
    }

    /**
     * @param UrlManager $manager
     * @param Request $request
     * @return array|bool
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = trim($request->pathInfo, "\t\n\r\0\x0B/");

        if ($pathInfo == '') {
            if (!$model = Content::findOne(1)) {
                return false;
            }

            Yii::$app->content->model = $model;

            return ['site/index', []];
        }

        $model = Content::findOne(['alias' => $pathInfo]);
        if ($model && $model->published == 1) {
            Yii::$app->content->model = $model;

            return [$model->type . '/' . ($model->isLeaf() && $model->depth > 1 ? 'view' : 'index'), []];
        }

        return false;
    }
}