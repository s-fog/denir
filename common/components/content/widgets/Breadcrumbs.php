<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 16.02.2016
 * Time: 17:05
 */

namespace common\components\content\widgets;

use Yii;

class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $homeLink = false;
    public $options = [
        'class' => 'breadcrumbs__list ul-reset',
    ];

    public $itemTemplate = '<li class="breadcrumbs__item">{link}</li> <li class="breadcrumbs__delimiter">></li> ';
    public $activeItemTemplate = '<li class="breadcrumbs__item">{link}</li>';

    public function init()
    {
        if (empty($this->links) && $model = Yii::$app->content->model) {
            foreach ($model->parents()->each() as $parent) {
                $this->links[] = [
                    'url' => $parent->type != 'category' && $parent->published != 0 ? $parent->uri : false,
                    'label' => $parent->menutitle,
                    'class' => 'breadcrumbs__link',
                ];
            }

            $this->links[] = $model->menutitle;
        }
    }

}