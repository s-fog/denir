<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 14.02.2016
 * Time: 19:46
 */

namespace common\components\content\widgets;


use common\components\content\models\Content;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


class Menu extends \yii\widgets\Menu
{
    public $parents = [1];
    public $only = [];
    public $except;
    public $level = 1;
    public $includeParents = false;
    public $labelTemplate = '<span>{label}</span>';
    public $activateParents = true;
    public $name = 'menu';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->options = ArrayHelper::merge(['class' => $this->name . ' ul-reset'], $this->options);
        $this->activeCssClass = $this->name . '__item_active';

        if ($this->only) {
            $this->parents = null;
        }

        /** @var Content $content */
        foreach (Content::find()->filterWhere(['id' => $this->parents])->orFilterWhere(['id' => $this->only])->orderBy(['lft' => SORT_ASC])->each() as $content) {
            if ($this->includeParents || $this->only) {
                $this->items[] = $this->addItem($content);
            }

            $this->items = array_merge($this->items, $this->only ? [] : $this->getChildren($content));
        }
    }

    /**
     * @param Content $parent
     * @return array
     */
    protected function getChildren($parent)
    {
        $items = [];

        foreach ($parent->children(1)->andFilterWhere(['not in', 'id', $this->except])->published()->inMenu()->each() as $item) {
            $items[] = $this->addItem($item);
        }

        return $items;
    }

    /**
     * @param Content $item
     * @return array
     */
    protected function addItem($item)
    {
        $result = [
            'label' => $item->menutitle,
            'items' => !$item->isRoot() && !$item->isLeaf() && $item->depth < $this->level ? $this->getChildren($item) : [],
            'options' => [
                'class' => $this->name . '__item' . ($item->depth == 3 ? ' ' . $this->name . '__item_inner' : '') . ' ' . $this->name . '__item_level_' . $item->depth,
            ],
            'template' => Html::a('{label}', '{url}', ['class' => $this->name . '__link' . ($item->depth == 3 ? $this->name . ' __link_inner' : '' . ' ' . $this->name . '__link_level_' . $item->depth)]),
            'submenuTemplate' => "\n<ul class='{$this->name}__inner {$this->name}__inner_level_$item->depth'>\n{items}\n</ul>\n",
        ];

        if ($item->isRoot()) {
            $result['url'] = ['site/index'];
        } else if ($item->type != 'category') {
            $result['url'] = ['content/view', 'alias' => $item->alias];
        }

        return $result;
    }

    protected function isItemActive($item)
    {
        if (isset($item['url'], $item['url'][0], $item['url']['alias']) && \Yii::$app->content !== null) {
            if ($item['url'][0] == 'content/view' && $item['url']['alias'] == \Yii::$app->content->alias) {
                return true;
            }
        }

        return parent::isItemActive($item);
    }


}