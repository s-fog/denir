<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 29.02.2016
 * Time: 19:48
 */

namespace common\components\content\widgets;


use unclead\widgets\MultipleInput;
use yii\base\Model;

class ContentSettingsInput extends MultipleInput
{
    public $columnClass = 'common\components\content\SingleInputColumn';

    protected function initData()
    {
        if (is_null($this->data) && $this->model instanceof Model) {
            $settings = $this->model->settings;
            if (isset($settings[$this->name])) {
                foreach ((array)$settings[$this->name] as $index => $value) {
                    $this->data[$index] = $value;
                }
            }
        }
    }

}