<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 14.02.2016
 * Time: 2:19
 */

namespace common\components\content;


use common\models\Content;
use yii\base\Object;
use yii\helpers\Html;

/**
 * Class ContentComponent
 *
 * @package frontend\components
 *
 * @property Content $model
 *
 * @property integer $id
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $parent_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $type
 * @property string $alias
 * @property string $pagetitle
 * @property string $menutitle
 * @property string $text
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $published
 * @property integer $hidemenu
 * @property integer $show_in_tree
 * @property integer $deleted
 *
 * @property string $datetime
 *
 */
class ContentComponent extends Object
{
    public $model;

    public function __isset($name)
    {
        if (isset($this->model) && $this->model->hasAttribute($name)) {
            return true;
        }

        return false;
    }

    public function __get($name)
    {
        if (isset($this->model) && ($this->model->hasAttribute($name) || $this->model->canGetProperty($name))) {
            return $this->model->$name;
        }

        if (isset($this->model, $this->model->typeModel) && ($this->model->typeModel->hasAttribute($name) || $this->model->typeModel->canGetProperty($name))) {
            return $this->model->typeModel->$name;
        }

        return false;
    }

    public function seoTags()
    {
        $model = $this->model;

        if ($model !== null) {
            if ($model->seo_description) {
                echo Html::tag('meta', '', ['name' => 'description', 'content' => $model->seo_description]);
            }
            if ($model->seo_keywords) {
                echo Html::tag('meta', '', ['name' => 'keywords', 'content' => $model->seo_keywords]);
            }
        }
    }
}