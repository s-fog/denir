<?php

namespace common\components\content;


use unclead\widgets\MultipleInputColumn;

class SingleInputColumn extends MultipleInputColumn
{
    /**
     * @param int|null $index
     * @param bool $withPrefix
     * @return string
     */
    public function getElementName($index, $withPrefix = true)
    {
        if (is_null($index)) {
            $index = '{' . $this->renderer->getIndexPlaceholder() . '}';
        }

        $elementName = '[' . $this->context->name . '][' . $index . '][' . $this->name . ']';
        $prefix = $withPrefix ? $this->getInputNamePrefix() : '';

        return $prefix . $elementName;
    }
}