<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 13.02.2016
 * Time: 11:19
 */

namespace common\components\content;

use common\models\Content;
use yii\db\ActiveQuery;

trait ContentInheritanceTrait
{
    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        if (\Yii::$app->id == 'app-frontend') {
            return parent::find()->joinWith('content')->andWhere(['and', ['content.published' => 1], ['content.deleted' => 0]]);
        }

        return parent::find();
    }

    public function behaviors()
    {
        return [
            ContentBehavior::className(),
        ];
    }

    public function rules()
    {
        return $this->content ? array_merge(
            $this->content->rules(),
            parent::rules()
        ) : parent::rules();
    }

    public function attributeLabels()
    {
        return $this->content ? array_merge(
            $this->content->attributeLabels(),
            parent::attributeLabels()
        ) : parent::attributeLabels();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(Content::className(), ['id' => 'id']);
    }
}
