<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 10.02.2016
 * Time: 23:11
 */

namespace common\components\content;


use yii\web\AssetBundle;

class JsTreeAssetBundle extends AssetBundle
{
    public $sourcePath = '@bower/jstree/dist';

    public $js = [
        'jstree.min.js',
    ];

    public $css = [
        'themes/default/style.min.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}