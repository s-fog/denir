<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 26.05.2015
 * Time: 11:06
 */

namespace common\components\uniform;


use frontend\models\Form;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

class UniForm extends ActiveForm
{
    public $action = ['site/sendform'];
    public $validateOnBlur = false;
    public $validateOnChange = false;
    public $options = ['class' => 'sendform'];

    public $type;

    public $formModel = 'frontend\models\Form';

    public $fieldTemplate = '{input}{error}';
    public $showPlaceholder = true;

    public $thanksText = 'Спасибо за заявку!<br>Мы свяжемся с Вами в течение 30 минут!';

    /**
     * @var Form $_model
     */
    private $_model;

    public function init()
    {
        if (empty($this->type)) {
            throw new InvalidConfigException('В конфигурации формы должен быть указан type!');
        }

        $this->_model = new $this->formModel(['type' => $this->type]);

        parent::init();
    }

    public function run()
    {
        echo Html::activeHiddenInput($this->_model, 'type');
        echo $this->field('BC');
        $this->registerScript();
        parent::run();
    }


    /**
     * @param string $attribute
     * @param array $options
     * @return \yii\widgets\ActiveField
     */
    public function field($attribute, $options = [])
    {
        $model = $this->_model;

        return parent::field($model, $attribute, ArrayHelper::merge($options, [
            'template' => $this->fieldTemplate,
            'inputOptions' => [
                'required' => $this->_model->isAttributeRequired($attribute),
                'placeholder' => $this->showPlaceholder ? $model->getAttributeLabel($attribute) : false,
            ],
        ]));
    }

    public function registerScript()
    {
        $js = <<<JS
$(document).on('submit', '.sendform, .sendFile', function (e) {
        var form = $(this);
        e.preventDefault();
        $.fancybox.showLoading();

        var formData = form.serialize();
        var processData = true;
        var contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        var filefield = form.find('input[type="file"]');
        if(filefield.length && filefield.val() != ''){
            formData = new FormData(form[0]);
            processData = false;
            contentType = false;
        }

        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            processData: processData,
            contentType: contentType,
            async: processData,
            success: function (data) {
                form.trigger('reset');
                $('p.filename').remove();
                $.fancybox.open('<h2 style="text-align:center;font-size:25px;">Спасибо за заявку!<br>Мы свяжемся с Вами в течение 30 минут! </h2>', {
                    scrolling: 'no',
                    openSpeed: 300,
                    minHeight: '5',
                    afterLoad: function () {
                        setTimeout(function () {
                            $.fancybox.close();
                        }, 5000);
                    },
                    padding: 20,
                    helpers: {
                        overlay: {
                            locked: false
                        }
                    }
                });
            }
        });
        return false;
    });
JS;
        $this->view->registerJs($js, View::POS_READY, 'uniform');
    }
}