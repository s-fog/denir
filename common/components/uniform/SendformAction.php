<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 03.03.2016
 * Time: 19:54
 */

namespace common\components\uniform;


use frontend\models\Form;
use Yii;
use yii\base\Action;
use yii\web\Response;

class SendformAction extends Action
{
    public function run()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = new Form();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return $model->sendForm();
            }

            return false;
        } else {
            return $this->controller->redirect('/');
        }
    }

}