<?php
return [
    'name' => 'ОткрытиЯ',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath'=>'@common/runtime/cache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings',
            'modelClass'=>'common\models\Settings',
        ],
    ],
    'language'=>'ru'
];
