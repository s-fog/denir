<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'backend\models\User',
            ],
            'enableFlashMessages' => false,
            'enableRegistration' => false,
            'enableConfirmation' => false,
            'enablePasswordRecovery' => false,
            'enableUnconfirmedLogin' => true,
            'emailChangeStrategy' => \dektrium\user\Module::STRATEGY_INSECURE,
            'admins' => ['dev', 'admin', 'valpamaxim'],
        ],
        'settings' => [
            'class' => 'backend\modules\settings\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'rules' => [
                'users/<action>' => 'users/default/<action>',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user',
                ],
            ],
        ],
    ],
    'controllerMap' => [
        'content' => 'common\components\content\controllers\ContentController',
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '',
                'basePath' => '@www',
                'path' => 'uploads',
                'name' => 'Файлы'
            ],
            /*'roots' => [
                [
                    'baseUrl'=>'',
                    'basePath'=>'@www',
                    'path' => 'images',
                    'name' => 'Изображения'
                ],
            ],*/
        ]
    ],
    'params' => $params,
];
