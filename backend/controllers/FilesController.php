<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 21.10.2015
 * Time: 9:31
 */

namespace backend\controllers;


use yii\filters\AccessControl;
use yii\web\Controller;

class FilesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}