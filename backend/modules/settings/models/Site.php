<?php


namespace backend\modules\settings\models;


use yii\base\Model;

class Site extends Model
{
    public $phone;
    public $youtube;
    public $linkedin;
    public $twitter;

    public function rules()
    {
        return [
            [
                ['phone', 'youtube', 'linkedin', 'twitter'], 'string',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => 'Федеральный номер',
        ];
    }
}