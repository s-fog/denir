<?php

/* @var \yii\web\View $this */
/* @var \backend\modules\settings\models\Site $model */

$this->title = 'Настройки';

?>

<div class="box box-default">
    <div class="box-body">
        <?= $this->render('_form', ['model' => $model]) ?>
    </div>
</div>
