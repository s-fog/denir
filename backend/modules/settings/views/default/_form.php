<?php

use yii\bootstrap\ActiveForm;

/* @var \yii\web\View $this */
/* @var \backend\modules\settings\models\Site $model */

?>

<?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-6">
            <fieldset>
                <legend>Контакты</legend>
                <?= $form->field($model, 'phone') ?>
            </fieldset>
            <fieldset>
                <legend>Социальные сети</legend>
                <?= $form->field($model, 'youtube') ?>
                <?= $form->field($model, 'linkedin') ?>
                <?= $form->field($model, 'twitter') ?>
            </fieldset>
        </div>
    </div>
    <button class="btn btn-primary">Сохранить</button>
<?php $form->end() ?>