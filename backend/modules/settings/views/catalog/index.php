<?php

use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Настройки каталога';
?>

<div class="row">
    <div class="col-md-6">
        <div class="box box-solid">
            <?php Pjax::begin(['enablePushState' => false]) ?>
            <div class="box-header">
                <h3 class="box-title">Характеристики</h3>
            </div>
            <div class="box-body">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'export' => false,
                    'columns' => [
                        [
                            'class' => \kartik\grid\EditableColumn::className(),
                            'attribute' => 'name',
                            'editableOptions' => ['formOptions' => ['action' => ['update']]],

                        ],
                        [
                            'class' => \yii\grid\ActionColumn::className(),
                            'template' => '{delete}',
                        ],
                    ],
                ]) ?>
            </div>
            <div class="box-footer">
                <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'action' => ['create'],
                    'layout' => 'inline',
                    'options' => [
                        'data-pjax' => 1,
                    ],
                ]) ?>
                <?= $form->field(new \common\models\Feature(), 'name') ?>
                <button class="btn btn-primary">Добавить</button>
                <?php $form->end() ?>
            </div>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>