<?php

use yii\bootstrap\Nav;

/* @var \yii\web\View $this */
/* @var string $content */

?>

<?php $this->beginContent('@backend/views/layouts/main.php') ?>

<div class="nav-tabs-custom">
    <?= Nav::widget([
        'items' => [
            [
                'label' => 'Сайт',
                'url' => ['/settings/default/index'],
            ],
            [
                'label' => 'Каталог',
                'url' => ['/settings/catalog/index'],
            ],
        ],
        'options' => [
            'class' => 'nav-tabs',
        ],
    ]) ?>

    <?= $content ?>
</div>

<?php $this->endContent() ?>
