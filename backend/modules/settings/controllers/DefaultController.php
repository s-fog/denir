<?php

namespace backend\modules\settings\controllers;

use yii\web\Controller;

/**
 * Default controller for the `settings` module
 */
class DefaultController extends Controller
{
    function actions()
    {
        return [
            'index' => [
                'class' => 'pheme\settings\SettingsAction',
                'modelClass' => 'backend\modules\settings\models\Site',
                'viewName' => 'index',
            ],
        ];
    }
}
