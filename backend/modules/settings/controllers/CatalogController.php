<?php


namespace backend\modules\settings\controllers;


use common\models\Feature;
use kartik\grid\EditableColumnAction;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CatalogController extends Controller
{
    public function actions()
    {
        return [
            'update' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Feature::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Feature::find()->indexBy('id'),
            'pagination' => false,
            'sort' => false,
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        $model = new Feature();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
        }

        if (Yii::$app->request->isPjax) {
            return $this->actionIndex();
        }

        return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Feature
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (!$model = Feature::findOne($id)) {
            throw new NotFoundHttpException();
        }

        return $model;
    }
}