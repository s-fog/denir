<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 21.10.2015
 * Time: 9:32
 */

use mihaildev\elfinder\ElFinder;

/* @var \yii\web\View $this */

$this->title = 'Загрузка файлов';
//\mihaildev\elfinder\Assets::noConflict($this);
?>

<div class="box box-solid">
    <div class="box-body">
        <?= ElFinder::widget([
            'language' => 'ru',
            'frameOptions' => [
                'style' => "width: 100%; height: 500px; border: 0;"
            ]
        ]) ?>
    </div>
</div>