<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 13.02.2016
 * Time: 5:40
 */

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var \yii\web\View $this */
/* @var \yii\bootstrap\ActiveForm $form */
/* @var \yii\db\ActiveRecord $model */

?>

<div class="row">
    <div class="col-md-9">
        <?= $form->field($model, 'pagetitle') ?>
        <?= $form->field($model, 'menutitle') ?>
        <?= $form->field($model, 'alias') ?>
        <?= $form->field($model, 'text')->widget(CKEditor::className(), [
            'editorOptions' => array_merge(
                ElFinder::ckeditorOptions([
                    'elfinder',
                ],
                    [
                        'preset' => 'standart',
                        'basicEntities' => false,
                        'entities' => false,
                        'entities_processNumerical' => false,
                        'htmlEncodeOutput' => false,
                        'baseHref' => '/',
                        'allowedContent' => true,
                    ]))
        ]) ?>
    </div>
    <div class="col-md-3 well">
        <fieldset>
            <legend>Настройки</legend>
            <?php if (Yii::$app->user->id == 1) {
                echo $form->field($model, 'type')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Content::find()->groupBy('type')->all(), 'type', 'type'));
            } ?>
            <?= $form->field($model, 'published')->checkbox() ?>
            <?= $form->field($model, 'hidemenu')->checkbox() ?>
        </fieldset>
    </div>
</div>
