<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 13.02.2016
 * Time: 1:15
 */

use common\components\content\JsTreeAssetBundle;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var string $content */

JsTreeAssetBundle::register($this);
$model = $this->params['model'];
?>

<?php $this->beginContent('@backend/views/layouts/main.php') ?>

<div class="row">
    <div class="col-md-3">
        <div class="box box-solid">
            <div class="box-body">
                <div id="content-tree"></div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-solid">
            <?php Pjax::begin([
                'id' => 'content-pjax',
                'formSelector' => 'form',
            ]) ?>

            <?= $content ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php $js = <<<JS
var tree = $('#content-tree');

tree.jstree({
    'plugins' : ['changed', 'wholerow', 'dnd', 'types', 'contextmenu'],
    'core' : {
        'multiple' : false,
        'data' : {
            url: function(node) {
                return '/manager/content/tree';
            },
            data: function(node) {
                return { 'id' : 1, 'content_id': $model->id };
            }
        },
        'check_callback' : function (operation, node, node_parent, node_position, more) {
            if(operation == 'move_node'){
                if(node.parent !== node_parent.id){
                    return false;
                }
            }

            return true;
        }
    },
    'massload' : {
        url: '/manager/content/tree',
        data: function(nodes) {
            return {"ids": nodes.join(",")};
        }
    },
    'contextmenu' : {
        select_node : false,
        items : {
            "create" : {
                "separator_before"	: false,
                "icon"              : "fa fa-plus",
                "separator_after"	: true,
                "_disabled"			: function(data){
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    return obj.id != 1;
                },
                "label"				: "Добавить",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    inst.create_node(obj, {}, "last", function (new_node) {
                        setTimeout(function () {
                            inst.edit(new_node, 'Новая страница', function(node, status, cancelled){
                                $.post('/manager/content/create?parent=' + node.parent, {pagetitle: node.text});
                            });
                        }, 0);
                    });

                }
            },
            "remove" : {
                "separator_before"	: false,
                "icon"				: "fa fa-trash",
                "separator_after"	: false,
                "_disabled"			: function(data){
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    return obj.id == 1;
                },
                "label"				: "Удалить",
                "action"			: function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);
                    if(inst.is_selected(obj)) {
                        inst.delete_node(inst.get_selected());
                        $.post('/manager/content/delete?id=' + obj.id, function(){
                            location.href = '/manager/content/update?id=' + obj.parent;
                        });
                    }
                    else {
                        inst.delete_node(obj);
                        $.post('/manager/content/delete?id=' + obj.id);
                    }


                }
            }
        }
    },
    'dnd' : {
        copy : false,
        is_draggable: function(nodes){
            if(nodes[0]['id'] == 1){
                return false;
            }

            return true;
        }
    }
});

tree
.on('select_node.jstree', function(event, data){
    var ref = tree.jstree(true);
    $.pjax.reload('#content-pjax', {url: '/manager/content/update?id=' + data.node.id});
    ref.close_all(ref.get_children_dom(data.node));
    ref.open_node(data.node);
})
.on('move_node.jstree', function(event, data){
    var post = {otherNodeKey: data.node.id};
    var ref = tree.jstree(true);

    if(data.position == 0){
        var nextNode = ref.get_next_dom(data.node, true);
        if(nextNode == false){
            post.mode = 'over';
            post.nodeKey = data.node.parent;
        } else {
            post.mode = 'before';
            post.nodeKey = $(ref.get_next_dom(data.node, true)).attr('id');
        }
    } else {
        post.mode = 'after';
        post.nodeKey = $(ref.get_prev_dom(data.node, true)).attr('id');
    }

    $.post('/manager/content/move', post);
})
.on('rename_node.jstree', function(event, data) {
    console.log(data);
})
;

JS;

$this->registerJs($js);
?>
<?php $this->endContent() ?>
