<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 21.11.2015
 * Time: 18:30
 */

use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;

/* @var \yii\web\View $this */
/* @var \common\models\Content $model */
/* @var \yii\data\ActiveDataProvider $contentDataProvider */


$this->title = 'Управление страницами';
$this->params['model'] = $model;
?>

<?php $form = ActiveForm::begin([
    'id' => 'content-form'
]) ?>

    <div class="box-header">
        <h3>Редактирование страницы "<?= $model->pagetitle ?>"
            <a href="../<?= $model->id == 1 ? '' : $model->alias ?>" class="btn btn-default pull-right" target="_blank" data-pjax="0"><i class="fa fa-eye"></i></a>
        </h3>
    </div>
    <div class="box-body">
        <?php
        $tabs = [];
        $tabs[] = [
            'label' => 'Страница',
            'content' => $this->render('_doc', ['form' => $form, 'model' => $model]),
        ];

        $tabs[] = [
            'label' => 'SEO',
            'content' => $this->render('_seo', ['form' => $form, 'model' => $model]),
        ];
        ?>

        <div class="nav-tabs-custom">
            <?= Tabs::widget([
                'items' => $tabs,
            ]) ?>
        </div>

    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary"><?= FA::i('save') ?>&nbsp;Сохранить</button>
    </div>

<?php $form->end() ?>