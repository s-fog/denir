<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 13.02.2016
 * Time: 5:43
 */

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var \yii\web\View $this */
/* @var \yii\bootstrap\ActiveForm $form */
/* @var \yii\db\ActiveRecord $model */

?>

<?= $form->field($model, 'seo_title') ?>
<?= $form->field($model, 'seo_description')->textarea() ?>
<?= $form->field($model, 'seo_keywords')->textarea() ?>
<?= $form->field($model, 'seo_text')->widget(CKEditor::className(), [
    'editorOptions' => array_merge(
        ElFinder::ckeditorOptions([
            'elfinder',
        ],
            [
                'preset' => 'standart',
                'basicEntities' => false,
                'entities' => false,
                'entities_processNumerical' => false,
                'htmlEncodeOutput' => false,
                'baseHref' => '/',
                'allowedContent' => true,
            ]))
]) ?>
