<aside class="main-sidebar">
    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' =>
                    array_merge(
                        [
                            ['label' => 'Меню сайта', 'options' => ['class' => 'header']],
                            ['label' => 'Структура сайта', 'icon' => 'fa fa-sitemap', 'url' => ['/content']],
                            ['label' => 'Пользователи', 'icon' => 'fa fa-users', 'url' => ['/user/admin']],
                            ['label' => 'Загрузка файлов', 'icon' => 'fa fa-download', 'url' => ['/files']],
                        ],
                        YII_ENV == 'dev' && Yii::$app->user->id == 1 ? [
                            ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                            ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                            ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                        ] : []
                    ),
            ]
        ) ?>
    </section>
</aside>
