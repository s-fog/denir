<?php
/**
 * Created by PhpStorm.
 * User: Ilya Butorin
 * Date: 10.01.2016
 * Time: 6:23
 */

namespace backend\components;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class EncodingFixBehavior extends Behavior
{
    public $attributes = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'encodeAttributes',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'encodeAttributes',
        ];
    }

    public function encodeAttributes()
    {
        foreach ($this->attributes as $attribute) {
            if (is_array($this->owner->{$attribute})) {
                $value = $this->array_map_recursive('rawurldecode', $this->owner->{$attribute});
                } else {
                $value = rawurldecode($this->owner->{$attribute});
                }

            $this->owner->{$attribute} = $value;
            }
    }


    private function array_map_recursive($fn, $arr)
    {
        $rarr = [];
        foreach ($arr as $k => $v) {
            $rarr[$k] = is_array($v)
                ? $this->array_map_recursive($fn, $v)
                : $fn($v); // or call_user_func($fn, $v)
        }

        return $rarr;
    }

}