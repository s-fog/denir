<?php


namespace backend\components;


use mihaildev\elfinder\InputFile;

class InputImage extends InputFile
{
    public $filter = 'image';
    public $template = '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>';
    public $options = ['class' => 'form-control'];
    public $buttonOptions = ['class' => 'btn btn-default'];
    public $path = 'images';
}